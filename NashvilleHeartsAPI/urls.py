"""NashvilleHeartsAPI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import static
from rest_framework.authtoken import views

from nashvillehearts.views import (root,
                                   UserRegisterView, ContactFormAjaxView,
                                   ProviderAccessRequestView,
                                   ProviderFileUploadAjaxView,
                                   ProviderCredentialUpdateView,
                                   BookmarkDecodeView, BookmarkEncodeView,
                                   NHMTokenAuthentication)


urlpatterns = [
    url(r'^$', root, name='root'),
    url(r'^admin/', admin.site.urls),
    url(r'^account/authenticate/', NHMTokenAuthentication.as_view(), name='account_authenticate'),
    url(r'^account/register/', UserRegisterView.as_view(), name='account_register'),
    url(r'^resources/', include('nashvillehearts.urls')),
    url(r'^account/request-access/', ProviderAccessRequestView.as_view(), name='account_request_access'),
    url(r'^account/media-push/', ProviderFileUploadAjaxView.as_view(), name='account_media_push'),
    url(r'^account/credentials-update/', ProviderCredentialUpdateView.as_view(), name='account_credentials_update'),
    url(r'^account/contact-us/', ContactFormAjaxView.as_view(), name='account_contact_us'),
    url(r'^bookmarks/encode/', BookmarkEncodeView.as_view(), name='bookmarks_encode'),
    url(r'^bookmarks/decode/', BookmarkDecodeView.as_view(), name='bookmarks_decode'),
]

urlpatterns += [
    url(r'^resources-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'NHM Administration'
admin.site.site_title = 'Nashville Hearts | Administration'

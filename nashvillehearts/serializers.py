from rest_framework import serializers
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from .models import Provider, Location, Event, UrgentNeed, Service, ClientType, VisitorLanguage, Faq


class UserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=120, required=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'name')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()

        name = validated_data['name']  # name = request.DATA['name']
        print('..in User Creation, name is {}'.format(name))

        post_save.send(User, instance=user, provider_name=name, dispatch_uid="account_attach_provider")

        return user


class ServiceSerializer(serializers.ModelSerializer):
    id = serializers.ModelField(model_field=Service()._meta.get_field('id'))

    class Meta:
        model = Service
        fields = ('id', 'name', 'icon', 'description')


class ClientTypeSerializer(serializers.ModelSerializer):
    id = serializers.ModelField(model_field=ClientType()._meta.get_field('id'))

    class Meta:
        model = ClientType
        fields = ('id', 'name', 'description')


class LocationSerializer(serializers.ModelSerializer):
    id = serializers.ModelField(required=False, model_field=Location()._meta.get_field('id'))
    # address2 = serializers.ModelField(required=False)

    class Meta:
        model = Location

    def create(self, validated_data):
        return Location.objects.create(**validated_data)


class ProviderSerializer(serializers.ModelSerializer):
    account = serializers.ReadOnlyField(source='account.username')
    locations = LocationSerializer(many=True, read_only=False)
    services = ServiceSerializer(many=True, read_only=False)
    client_types = ClientTypeSerializer(many=True, read_only=False)

    class Meta:
        model = Provider
        fields = ('id', 'account', 'name', 'description', 'avatar', 'org_type',
                  'phone', 'email', 'website', 'facebook', 'twitter', 'locations', 'client_types',
                  'services', 'is_faith_based', 'volunteer_contact_name', 'volunteer_contact_email',
                  'volunteer_contact_phone', 'is_public')

    def update(self, instance, validated_data):
        print(validated_data)
        services = validated_data.pop('services') if 'services' in validated_data else []
        locations = validated_data.pop('locations') if 'locations' in validated_data else []
        client_types = validated_data.pop('client_types') if 'client_types' in validated_data else []
        print(instance)

        obj = super(ProviderSerializer, self).update(instance, validated_data)

        if services:
            obj.services.clear()
            for srvc in services:
                print(repr(srvc))
                service_qs = Service.objects.filter(id=srvc["id"])

                if service_qs.exists():
                    service = service_qs.first()
                    obj.services.add(service)

        if client_types:
            obj.client_types.clear()
            for ctype in client_types:
                print(repr(ctype))
                ctype_qs = ClientType.objects.filter(id=ctype["id"])

                if ctype_qs.exists():
                    client_type = ctype_qs.first()
                    obj.client_types.add(client_type)

        if locations:
            # obj.locations.clear()
            for loc in locations:
                print(repr(loc))
                location_qs = Location.objects.filter(id=loc["id"])
                # loc, created = Location.objects.get_or_create(**loc)

                if location_qs.exists():
                    location = location_qs.first()
                    obj.locations.add(location)

        print(obj)
        print(obj.locations)

        # obj.locations = locations
        obj.save()

        return obj


class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('id', 'provider', 'event_host', 'title', 'description', 'start_datetime', 'end_datetime',
                  'location', 'event_type', 'event_url', 'event_image', 'event_address1', 'event_address2',
                  'event_city', 'event_state', 'event_zip', 'event_starttime', 'event_endtime',
                  'event_month', 'event_day_of_month', 'event_year', 'event_coordinates',
                  'address1', 'address2', 'city', 'state', 'zip')

    def create(self, validated_data):
        return Event.objects.create(**validated_data)


class UrgentNeedSerializer(serializers.ModelSerializer):

    class Meta:
        model = UrgentNeed
        fields = ('id', 'provider', 'event_host', 'title', 'description', 'location', 'event_type', 'event_url',
                  'event_image', 'event_address1', 'event_address2', 'event_city', 'event_state',
                  'event_zip', 'start_datetime', 'end_datetime', 'expiration_date', 'event_month',
                  'event_day_of_month', 'event_year', 'event_coordinates', 'time_left',
                  'time_left_units', 'address1', 'address2', 'city', 'state', 'zip')

    def create(self, validated_data):
        return UrgentNeed.objects.create(**validated_data)


class VisitorLanguageSerializer(serializers.ModelSerializer):
    # id = serializers.ModelField(model_field=VisitorLanguage()._meta.get_field('id'))

    class Meta:
        model = VisitorLanguage
        # fields = ('id', 'iso_code', 'country')


class FaqSerializer(serializers.ModelSerializer):
    id = serializers.ModelField(model_field=Faq()._meta.get_field('id'))

    class Meta:
        model = Faq
        fields = ('id', 'question', 'answer', 'modified')

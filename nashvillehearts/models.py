import uuid
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone, datetime_safe
from django.utils.text import slugify
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.conf import settings
from rest_framework.authtoken.models import Token

from datetime import timedelta

from .gmappy.gmapgate import GmapGate
from .mailer.mailgate import MailGate


# Nashville Hearts Models

GMAP = GmapGate()


# Helper Object
class DummyRequest(object):

    def get_host(self):
        host = settings.NHM_ROOT_URL if settings and settings.NHM_ROOT_URL else None
        if host and host.find("//") > -1:
            _, host = host.split("//", 1)
        return host

    def get_protocol(self):
        if settings.NHM_TEST_MODE:
            return "http://"
        else:
            return "https://"

    def get_web_client_url(self):
        web_client_url = settings.NHM_WEB_CLIENT_URL if settings and settings.NHM_WEB_CLIENT_URL else None
        return web_client_url


class Location(models.Model):
    """
        Location Model
    """
    US_STATES = (
        ('TN', 'Tennessee'),
    )
    #  ('Alabama', 'AL'),
    #  ('Georgia', 'GA'),
    #  ('Florida', 'FL'),
    #  ('Kentucky', 'KY'),
    #  ('Mississippi', 'MS'),

    name = models.CharField(max_length=120)
    address1 = models.CharField(max_length=120)
    address2 = models.CharField(max_length=60, blank=True, null=True)
    city = models.CharField(max_length=100, default='Nashville')
    state = models.CharField(
        max_length=2,
        choices=US_STATES,
        default='TN'
    )
    zip = models.BigIntegerField(blank=True, null=True)
    description = models.TextField(max_length=256, blank=True, null=True)

    latitude = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        super(Location, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return "{0}: {1}".format(self.name, self.address1)


class Service(models.Model):
    """
        Service Model
    """
    name = models.CharField(max_length=60)
    icon = models.ImageField(upload_to='service_logos')
    description = models.TextField(max_length=256, blank=True, null=True)

    def __str__(self):
        return "{0}".format(self.name)


class ClientType(models.Model):
    """
        ClientType Model
    """
    name = models.CharField(max_length=60)
    description = models.TextField(max_length=256, blank=True, null=True)

    def __str__(self):
        return "{0}".format(self.name)


class Provider(models.Model):
    """
        Provider Model
    """
    NONPROFIT = 'nonprofit'
    GOVERNMENT = 'government'
    COMMUNITY = 'community'
    VOLUNTEER = 'volunteer'
    ORGANIZATION_TYPES = (
        (NONPROFIT, 'Non-Profit'),
        (GOVERNMENT, 'Government'),
        (COMMUNITY, 'Community'),
        (VOLUNTEER, 'Volunteer')
    )
    account = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=120, null=True)
    description = models.TextField(max_length=512, blank=True, null=True)
    avatar = models.ImageField(upload_to='provider/logos', null=True, blank=True)
    org_type = models.CharField(
        max_length=12,
        choices=ORGANIZATION_TYPES,
        default=NONPROFIT
    )

    slug = models.SlugField(unique=True, blank=True, null=True)

    phone = models.CharField(max_length=15, blank=True, null=True)

    website = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    facebook = models.URLField(blank=True, null=True)

    locations = models.ManyToManyField(Location, blank=True)
    services = models.ManyToManyField(Service, blank=True)
    client_types = models.ManyToManyField(ClientType, blank=True)

    is_faith_based = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    is_approved = models.BooleanField(default=False)
    welcomed = models.BooleanField(default=False)

    is_public = models.BooleanField(default=True, help_text="public provider profiles will appear on the resource map")

    volunteer_contact_name = models.CharField(max_length=120, null=True, blank=True)
    volunteer_contact_email = models.CharField(max_length=150, null=True, blank=True)
    volunteer_contact_phone = models.CharField(max_length=15, null=True, blank=True)

    @property
    def email(self):
        return self.account.email if self.account else "N/A"

    @property
    def username(self):
        return self.account.username if self.account else "N/A"

    @property
    def is_active(self):
        return self.account.is_active if self.account else False

    @property
    def last_login(self):
        return self.account.last_login if self.account else "N/A"

    @property
    def created(self):
        return self.account.date_joined if self.account else "N/A"

    @property
    def fullname(self):
        return "{} {}".format(self.account.first_name, self.account.last_name) if self.account else "N/A"

    def __str__(self):
        return "{0} ({1})".format(self.name, self.org_type)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.account and not self.welcomed and (self.is_approved is True):
            # create random password for user account
            temporary_password = User.objects.make_random_password()
            self.account.set_password(temporary_password)
            self.account.save()
            # send welcome email ...
            req = DummyRequest()
            mailer = MailGate()
            mailer.sendmail('PROVIDER.WELCOME',
                            self.email,
                            {'organization': self.name,
                             'password': temporary_password,
                             'email': self.email,
                             'contact_name': self.fullname
                             }, request=req)
            #  then...
            self.welcomed = True

        if not self.slug and (self.id and self.name):
            # create a slug
            self.slug = slugify("r{} {}".format(self.id, self.name))

        super(Provider, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        ordering = ['name', 'account__last_login']


class Event(models.Model):
    """
        Event Model
    """
    GENERAL = 'general'
    URGENT_NEED = 'urgent_need'
    EVENT_TYPES = (
        (GENERAL, 'General'),
        (URGENT_NEED, 'Urgent Need'),
    )
    provider = models.ForeignKey(Provider, blank=True, null=True)
    title = models.CharField(max_length=60)
    description = models.CharField(max_length=512, null=True)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField(null=True, blank=True)
    location = models.ForeignKey(Location, blank=True, null=True)
    event_type = models.CharField(max_length=25, choices=EVENT_TYPES, default=GENERAL)
    event_url = models.CharField(max_length=255, blank=True)

    event_image = models.ImageField(upload_to='events', null=True, blank=True)

    address1 = models.CharField(max_length=120, blank=True)
    address2 = models.CharField(max_length=60, blank=True)
    city = models.CharField(max_length=100, default='Nashville')
    state = models.CharField(
        max_length=2,
        choices=Location.US_STATES,
        default='TN'
    )
    zip = models.BigIntegerField(null=True, blank=True)

    longitude = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    @property
    def event_starttime(self):
        return self.start_datetime.strftime("%I:%M %p")

    @property
    def event_endtime(self):
        if self.end_datetime:
            return self.end_datetime.strftime("%I:%M %p")
        return "N/A"

    @property
    def event_year(self):
        return self.start_datetime.strftime("%Y")

    @property
    def event_month(self):
        return self.start_datetime.strftime("%b")

    @property
    def event_day_of_month(self):
        return self.start_datetime.day

    @property
    def event_host(self):
        if self.provider:
            return self.provider.name

        return "N/A"

    @property
    def event_host_id(self):
        if self.provider:
            return self.provider.id

        return -1

    @property
    def event_address1(self):
        if self.location:
            return self.location.address1

        return self.address1

    @property
    def event_address2(self):
        if self.location:
            return self.location.address2

        return self.address2

    @property
    def event_city(self):
        if self.location:
            return self.location.city

        return self.city

    @property
    def event_state(self):
        if self.location:
            return self.location.state

        return self.state

    @property
    def event_zip(self):
        if self.location:
            return self.location.zip

        return self.zip

    @property
    def event_coordinates(self):
        if self.location:
            return self.location.longitude, self.location.latitude

        return self.longitude, self.latitude

    def __str__(self):
        return "Event: {}".format(self.title)


class UrgentNeed(Event):

    NEED_LIFESPAN = 48  # hours

    is_fulfilled = models.BooleanField(default=False)
    # expiration_date = models.DateTimeField(blank=True, null=True)

    @property
    def expiration_date(self):
        return self.end_datetime

    @property
    def time_left(self):
        if not self.end_datetime:
            return self.NEED_LIFESPAN

        today = timezone.now()
        time_left = self.end_datetime - today
        if time_left > timedelta(hours=self.NEED_LIFESPAN):
            return time_left // timedelta(days=1)  # days
        else:
            return time_left // timedelta(hours=1)  # hours

    @property
    def time_left_units(self):
        if not self.end_datetime:
            return "hours"

        today = timezone.now()
        time_left = self.end_datetime - today
        if time_left > timedelta(hours=self.NEED_LIFESPAN):
            return "days"  # days
        else:
            return "hours"  # hours

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.start_datetime:
            self.start_datetime = timezone.now()

        if not self.end_datetime:
            print("No end_datetime?? {}".format(self.end_datetime))
            self.end_datetime = self.start_datetime + timedelta(hours=self.NEED_LIFESPAN)
            print("Updated end_datetime?? {}".format(self.end_datetime))

        self.event_type = self.URGENT_NEED

        super(UrgentNeed, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return "Urgent Need: {}".format(self.title)


class SavedSearch(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    search_parameters = models.TextField()
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class VisitorLanguage(models.Model):
    iso_code = models.CharField(max_length=8)
    language = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return "{}: {}".format(self.language, self.iso_code)


class Faq(models.Model):
    question = models.CharField(max_length=255)
    answer = models.TextField()
    is_public = models.BooleanField(default=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Faq Q.{}".format(self.id)


# ################# SIGNALS ####################### #
@receiver(post_save, sender=Location)
def retrieve_geo_coordinates(sender, **kwargs):
    # kwargs.get('created', False)
    instance = kwargs.get('instance')
    if not (instance.longitude and instance.latitude) and instance.address1 and instance.city and instance.state:
        coords = GMAP.geocode("{}, {}, {}".format(instance.address1, instance.city, instance.state))
        print("just updated coordinates for {}: {}".format(instance.name, coords))
        if coords:
            instance.latitude, instance.longitude = coords
            instance.save()


@receiver(post_save, sender=Event)
def retrieve_event_missing_geo_coordinates(sender, **kwargs):
    # kwargs.get('created', False)
    instance = kwargs.get('instance')
    if not instance.location and not (instance.longitude and instance.latitude) and instance.address1 and instance.city and instance.state:
        coords = GMAP.geocode("{}, {}, {}".format(instance.address1, instance.city, instance.state))
        print("just updated Event's coordinates for {}: {}".format(instance, coords))
        if coords:
            instance.latitude, instance.longitude = coords
            instance.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        # create the user's token
        Token.objects.create(user=instance)
        print('user token created for {}'.format(instance))


@receiver(post_save, sender=User, dispatch_uid="account_attach_provider")
def create_provider(sender, instance=None, created=False, **kwargs):
    print("Checking to create provider ...{} {}".format(instance, repr(kwargs)))
    provider_name = kwargs.get('provider_name', None)
    if provider_name:
        # create the user's associated provider account
        Provider.objects.get_or_create(account=instance, name=provider_name)
        print('provider created for {}'.format(instance))


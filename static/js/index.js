/**
 * Created by sada on 10/10/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Main from './main';

injectTapEventPlugin();	//must be called to enable onTouchTap event

ReactDOM.render(<Main/>, document.getElementById('app-container'));
/**
 * Created by sada on 12/30/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';

export default class Marker extends React.Component {

    constructor(props, context) {
        super(props, context);
    }


    render() {
        return null;
    }

    componentDidMount() {
        this.renderMarker();
    }

    componentDidUpdate(prevProps) {
        //console.log("props.map", this.props.map, prevProps.map);
        //console.log("props.position", this.props.position, prevProps.position);
        //console.log("props.google", this.props.google, prevProps.google);
        if( (prevProps.map !== this.props.map) || (prevProps.position !== this.props.position) ) {
            console.log("about to re-render a marker...");
            this.renderMarker();
        }
        //console.log("...end of this marker attemp...");
    }

    componentWillUnmount() {
        this.removeMarker();
    }

    removeMarker() {
        if (this.marker) {
            this.marker.setMap(null);
        }
    }

    renderMarker() {
        const evtNames = ['click', 'mouseover'];

        let {map, google, position, mapCenter} = this.props;

        let point = position || mapCenter;
        point = new google.maps.LatLng(point.lat, point.lng);

        const params = {
            map: map,
            position: point
        };

        this.marker = new google.maps.Marker(params);
        console.log("rendered marker at: ", this.props.position);

        evtNames.forEach((evt) => {
           this.marker.addListener(evt, this.handleEvent(evt));
        });
    }

    handleEvent(evtName) {
        let handlerName = "on"+camelize(evtName);

        // call the handler which should be on the props object
        return (e) => {
            if(this.props[handlerName]) {
                this.props[handlerName](this.props, this.marker, e);
            } else {
                console.log('Could not find handler: ' + handlerName);
            }
        };
    }
}

Marker.PropTypes = {
    position: React.PropTypes.object,
    map: React.PropTypes.object
};

Marker.defaultProps = {
    onMouseover: (props, marker, e) => {
        console.log("marker received mouseover:", marker, e);
    },
    onClick: (props, marker, e) => {
        console.log("marker was clicked: ", props, marker, e);
    }
};

// Helper functions
        function camelize(str) {
            return str.split(' ').map(function(word){
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join('');
        }